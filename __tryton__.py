# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Invoice Cash Basis',
    'name_de_DE': 'Fakturierung Gültigkeitsdauer Umsatzsteuerliche Istversteuerung',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Cash Basis Accounting for Value Added Taxes (VAT)
        - Adds tax rules for cash basis accounting for VAT per fiscal year.
        - Adds tax attributes for cash basis accounting for VAT
    ''',
    'description_de_DE': '''Umsatzsteuerliche Istversteuerung
    - Fügt Steuerregeln für umsatzsteuerliche Istversteuerung per
      Geschäftsjahr hinzu.
    - Fügt Einstellungen für umsatzsteuerliche Istversteuerung den
      Steuern hinzu.
    ''',
    'depends': [
        'account_tax_cash_basis',
        'account_timeline_invoice',
    ],
    'xml': [
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}
