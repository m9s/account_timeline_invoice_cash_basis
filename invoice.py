#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from decimal import Decimal
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class Invoice(ModelSQL, ModelView):
    _name = 'account.invoice'

    def pay_invoice(self, invoice_id, amount, journal_id, date, description,
            amount_second_currency=False, second_currency=False):
        '''
        Adds a payment to an invoice and handle taxes for cash accounting.

        :param invoice_id: the invoice id
        :param amount: the amount to pay
        :param journal_id: the journal id for the move
        :param date: the date of the move
        :param description: the description of the move
        :param amount_second_currency: the amount in the second currenry if one
        :param second_currency: the id of the second currency
        :return: the id of the payment line
        '''
        pool = Pool()
        journal_obj = pool.get('account.journal')
        move_obj = pool.get('account.move')
        period_obj = pool.get('account.period')

        lines = []
        invoice = self.browse(invoice_id)
        journal = journal_obj.browse(journal_id)

        if invoice.type in ('out_invoice', 'in_credit_note'):
            lines.append({
                'name': description,
                'account': invoice.account.id,
                'party': invoice.party.id,
                'debit': Decimal('0.0'),
                'credit': amount,
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            lines.append({
                'name': description,
                'account': journal.debit_account.id,
                'party': invoice.party.id,
                'debit': amount,
                'credit': Decimal('0.0'),
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            if invoice.account.id == journal.debit_account.id:
                self.raise_user_error('same_debit_account')
            if not journal.debit_account:
                self.raise_user_error('missing_debit_account')
            for invoice_tax in invoice.taxes:
                if invoice_tax.tax.is_undue:
                    self.append_due_lines_from_tax(amount, lines,
                            invoice_tax.id)
        else:
            lines.append({
                'name': description,
                'account': invoice.account.id,
                'party': invoice.party.id,
                'debit': amount,
                'credit': Decimal('0.0'),
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            lines.append({
                'name': description,
                'account': journal.credit_account.id,
                'party': invoice.party.id,
                'debit': Decimal('0.0'),
                'credit': amount,
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            if invoice.account.id == journal.debit_account.id:
                self.raise_user_error('same_credit_account')
            if not journal.credit_account:
                self.raise_user_error('missing_credit_account')

        period_id = period_obj.find(invoice.company.id, date=date)
        ## Legacy refactoring?
        #move_id = move_obj.create({
        #    'journal': journal.id,
        #    'period': period_id,
        #    'date': date,
        #    'lines': [('create', x) for x in lines],
        #    })

        #move = move_obj.browse(move_id)

        #for line in move.lines:
        #    if line.account.id == invoice.account.id:
        #        self.write(invoice.id, {
        #            'payment_lines': [('add', line.id)],
        #            })
        #        return line.id
        #raise Exception('Missing account')

    def append_due_lines_from_tax(self, amount, lines=None,
            invoice_tax_id=None):
        invoice_tax_obj = Pool().get('account.invoice.tax')
        tax_obj = Pool().get('account.tax')

        if lines is None:
            lines = []

        invoice_tax = invoice_tax_obj.browse(invoice_tax_id)
        due_tax = invoice_tax.tax.due_tax
        lines = invoice_tax_obj.get_move_line(invoice_tax)

Invoice()


class Line(ModelSQL, ModelView):
    _name = 'account.invoice.line'

    def __init__(self):
        super(Line, self).__init__()
        self.product = copy.copy(self.product)
        if self.product.on_change is None:
            self.product.on_change = []
        if '_parent_invoice.invoice_date' not in self.product.on_change:
            self.product.on_change += ['_parent_invoice.invoice_date']
        if '_parent_invoice.accounting_date' not in self.product.on_change:
            self.product.on_change += ['_parent_invoice.accounting_date']
        if 'company' not in self.product.on_change:
            self.product.on_change += ['_parent_invoice.company']
        self._reset_columns()

    def _get_tax_rule_pattern(self, party, vals):
        '''
        Get tax rule pattern

        :param party: the BrowseRecord of the party
        :param vals: a dictionary with value from on_change
        :return: a dictionary to use as pattern for tax rule
        '''
        fiscalyear_obj = Pool().get('account.fiscalyear')

        res = super(Line, self)._get_tax_rule_pattern(party, vals)

        date = vals.get('_parent_invoice.effective_date')

        company_id = vals.get('_parent_invoice.company')
        fiscalyear_id = fiscalyear_obj.find(company_id, date)
        fiscalyear = fiscalyear_obj.browse(fiscalyear_id)
        res['taxation_method'] = False
        if fiscalyear:
            res['taxation_method'] = fiscalyear.taxation_method
        return res

Line()
